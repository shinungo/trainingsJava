package arrayTest;

public class ArrayTest1 {

	public static void main(String[] args) {

		// Array of Strings

		int[] zahlenArray = new int[10];

		for (int i = 0; i < zahlenArray.length; i++) {
			zahlenArray[i] = i * 100;
		}

		for (int i = 0; i < zahlenArray.length; i++) {
			System.out.print(zahlenArray[i] + " "); 
		}


		System.out.println();
		String[] tage = { "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So" }; 
		for (int i = 0; i < tage.length; i++) {
			System.out.print(tage[i] + " "); 
			}
	} 

}