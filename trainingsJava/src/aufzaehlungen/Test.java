package aufzaehlungen;

public class Test {

	public static void info(Ampelfarbe farbe) {
		switch (farbe) {
		case ROT:
			System.out.println(farbe.ordinal() + ": Anhalten");
			break;

		case GELB:
			System.out.println(farbe.ordinal() + ": Achtung");
			break;

		case GRUEN:
			System.out.println(farbe.ordinal() + ": Weiterfahren");
			break; 
		}
	}
	public static void main(String[] args) { 
		info(Ampelfarbe.ROT); 
		info(Ampelfarbe.GELB); 
		info(Ampelfarbe.GRUEN);

		for (Ampelfarbe farbe : Ampelfarbe.values()) {
			System.out.println(farbe.toString()); }
	}
}