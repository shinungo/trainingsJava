package interfacesBspVier;

public class B implements X {

	public static void main(String[] args) {
		B b = new B();

		// Folgende Aufrufe sind nicht möglich: // b.m();
		// B.m();
		X.m(); }
}