package Kap_3_Buch_Uebung;

public class U_05_Abschreibung {
	

	private double anschaffungspreis; 
	private int nutzungsjahre;
	private double abschreibungssatz;
	
	public U_05_Abschreibung() {}
	
	public U_05_Abschreibung (double anschaffungspreis, int nutzungsjahre, double abschreibungssatz) {
		this.anschaffungspreis = anschaffungspreis;
		this.nutzungsjahre = nutzungsjahre;
		this.abschreibungssatz = abschreibungssatz;
	}

	public void abschreibungGeometrisch() {
		double buchwert = anschaffungspreis;
		
		for (int i = 0; i < nutzungsjahre; i++)
			
		{
		 buchwert = buchwert * ((100-abschreibungssatz)/100);
		
		
		
		System.out.println("Jahr " + (i+1) + " Buchwert " + buchwert);
		}
	}
	
	public void abschreibungLinear() {
		double buchwert = anschaffungspreis;
	
		for (int i = 0; i < nutzungsjahre;i++) {
			buchwert = buchwert - (anschaffungspreis / nutzungsjahre);
			System.out.println("Jahr " + (i+1) + " Buchwert " + buchwert);
		}
	}
	
	public static void main (String[] args) {
		U_05_Abschreibung abschreibung = new U_05_Abschreibung(1000,10,1);
		
		abschreibung.abschreibungLinear();
		System.out.println();
		abschreibung.abschreibungGeometrisch();
		
	}
}
