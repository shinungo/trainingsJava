package Kap_3_Buch_Uebung;

public class U_25_Test {

    public static void main(String[] args) {
    	U_25_Implementierer r1 = new U_25_Implementierer(2, 3);
    	U_25_Implementierer r2 = new U_25_Implementierer(3, 2);
    	U_25_Implementierer r3 = new U_25_Implementierer(3, 3);
        
        System.out.println(r1.isQuadrat());
        System.out.println(r2.isQuadrat());
        System.out.println(r3.isQuadrat());
        
        System.out.println(U_25_Rechteck.compare(r1, r2));
        System.out.println(U_25_Rechteck.compare(r1, r3));
    }
}