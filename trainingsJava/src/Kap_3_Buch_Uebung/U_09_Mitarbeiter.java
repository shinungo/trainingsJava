package Kap_3_Buch_Uebung;

public abstract class U_09_Mitarbeiter {
	
	/*
	 * S. 104 im BUch - 2016_Book_GrundkursJAVA!!
	 */
	
	protected String nachname;
	protected String vorname;
	protected double gehalt;
	

	public U_09_Mitarbeiter(String nachname, String vorname, double gehalt) {
		this.nachname = nachname;
		this.vorname = vorname;
		this.gehalt = gehalt;
	}

	// Erhoehung des Gehalts um betrag
	public void erhoeheGehalt(double betrag) {
		gehalt += betrag;
	}

	// Ausgabe aller Variableninhalte
	public void zeigeDatenM() {
	System.out.println("Name: " + nachname + ","+ vorname);
	System.out.println("Gehalt: " + gehalt);
	}

	
	     public abstract void addZulage(double betrag);

}
