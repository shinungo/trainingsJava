package Kap_3_Buch_Uebung;

public interface U_25_Rechteck {
	
	

	int getBreite();
	int getHoehe();
	
	
	
	default public boolean isQuadrat() {
		return getBreite() == getHoehe(); 		
	};
	
	
	
	
	public static int compare(U_25_Rechteck a, U_25_Rechteck b) {
		int fa = a.getBreite() * a.getHoehe();
		int fb = b.getBreite() * b.getHoehe(); 
		if (fa > fb) return -1;
		else if (fa == fb) return 0;
		else
			return 1;

		
	}
	
	
}
