package Kap_3_Buch_Uebung;

public class U_21_Verzinsung1 {
	
	public static void main(String[] args) {
		System.out.println(zinsen(1000, 0.1, 2));
	}
	
	// Klassenmethode:
	
	
	public static double zinsen(double kapital, double zinssatz, int jahre) {
		for (int i = 0; i < jahre; i++) {
			System.out.println("Schlaufe um Zins pro Jahr zu rechnen");
			kapital *= (1 + zinssatz); 
			
		}
		System.out.println("Schluss: Kaptial zzgl. Zins");
		return kapital; 
	}

}
