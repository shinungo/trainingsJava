package Kap_3_Buch_Uebung;

public class U_06_Beleg {

	
	/**
	 * Erstellen Sie die Klasse Beleg, 
	 * deren Objekte bei ihrer Erzeugung automatisch 
	 * eine bei der Zahl 10000 beginnende laufende Belegnummer erhalten. 
	 * 
	 * Tipp: Verwenden Sie eine Klassenvariable.
	 */
	
	
	private static int BELEGNUMMER = 10000; 
	
	
	public U_06_Beleg(int BELEGNUMMER) {
		this.BELEGNUMMER = BELEGNUMMER;
	}
	
	public static void createBeleg() {
		
		BELEGNUMMER++;
		System.out.println("Beleg Nr. " + BELEGNUMMER);
		
	}
	
	
	public static void main (String[] args) {
		U_06_Beleg beleg = new U_06_Beleg(BELEGNUMMER);
		for (int i = 0; i<5000; i++)
		createBeleg();
	}
}