package Kap_3_Buch_Uebung;


public interface U_12_IntegerList {
	int getLength();
	void insertLast(int value);
	int getFirst();
	void deleteFirst();
	boolean search(int value);
}