package Kap_3_Buch_Uebung;

public class U_13_Test {
	public static void main(String[] args) {
		U_13_Stack stapel = new U_13_Stack();

		for (int i = 0; i < 20; i++) {
			stapel.push(100 + i);
		}

		for (int i = 0; i < 20; i++) {
			System.out.println(stapel.pop());
		}
	}
}