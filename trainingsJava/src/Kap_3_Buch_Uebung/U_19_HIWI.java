package Kap_3_Buch_Uebung;

public class U_19_HIWI implements U_19_StudHilfskraft{
	private String name;
	private double gehalt;
	private int matrNr;
	private int dauer;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getGehalt() {
		return gehalt;
	}

	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}

	public int getMatrNr() {
		return matrNr;
	}

	public void setMatrNr(int matrNr) {
		this.matrNr = matrNr;
	}

	public int getDauer() {
		return dauer;
	}

	public void setDauer(int dauer) {
		this.dauer = dauer;
	}

	public String toString() {
		return "HiWi [name=" + name + ", matrNr=" + matrNr + ", gehalt="
				+ gehalt + ", dauer=" + dauer + "]";
	}
}