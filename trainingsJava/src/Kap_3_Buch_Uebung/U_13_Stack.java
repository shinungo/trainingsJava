package Kap_3_Buch_Uebung;

public class U_13_Stack {

	private static final int INITIAL_CAPACITY = 16; 
	private int[] elements;
	private int size;  // zeigt auf next freien Platz
	
	public U_13_Stack() {
		elements = new int[INITIAL_CAPACITY];

	}

	
	public void push(int e) {
	ensureCapacity(); 
	elements[size++] = e;
	}

	public int pop() {
		return elements[--size];
	}
	
	private void ensureCapacity() {
		if (elements.length == size) {
			int[] neuesArray = new int[(2 * size)];

			for (int i = 0; i < size; i++) {
				neuesArray[i] = elements[i];
			}

			elements = neuesArray;
		}
	}
	
}
