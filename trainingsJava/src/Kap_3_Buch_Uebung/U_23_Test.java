package Kap_3_Buch_Uebung;

public class U_23_Test {
	
	public static void main (String[] args) {
		U_23_Queue queue = new U_23_Queue();
		
		for (int i = 0; i <= 30; i++) {
			queue.enter(i);
			System.out.println(i + " ");
			
		}
		
		System.out.println();
		int x;
		while ((x = queue.leave()) != -1){
			System.out.println(x + " ");
			
			
		}
		
		System.out.println();
	}

}
