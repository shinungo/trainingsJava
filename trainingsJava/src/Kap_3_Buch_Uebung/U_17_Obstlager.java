package Kap_3_Buch_Uebung;

public class U_17_Obstlager {
	
	private U_17_Obst[] lager = {new U_17_Orange(), new U_17_Birne(), new U_17_Apfel()};
	
	public void print() {
		for (int i = 0; i < lager.length; i++)
			System.out.println(lager[i].getName() + " " + lager[i].getFarbe());
		
	}
	
	
	
	public static void main(String [] args) {
		U_17_Obstlager obstlager = new U_17_Obstlager(); 
		obstlager.print(); 
		
	}

}
