package Kap_3_Buch_Uebung;

public class U_09_Angestellter extends U_09_Mitarbeiter {

	private static final int MAX_STUFE = 5;
	private int stufe;

	public U_09_Angestellter(String nachname, String vorname, double gehalt) {
		super(nachname, vorname, gehalt);

	}
	public void befoerdere() {
		stufe += 1;
	}


	// Ausgabe aller Variableninhalte
	public void zeigeDaten() {

		System.out.println("Angestellter ");
		super.zeigeDatenM();
		System.out.println("Stufe : " + stufe);
		System.out.println();
	}


	public void addZulage(double betrag) {
		if (stufe > 3) {
			super.erhoeheGehalt(betrag); 	 
		}
	}
}
