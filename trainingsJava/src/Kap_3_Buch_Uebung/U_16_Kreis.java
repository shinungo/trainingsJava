package Kap_3_Buch_Uebung;

public class U_16_Kreis extends U_16_Figur {
	
	private int x, y, radius; 
	
	public U_16_Kreis(int x, int y, int radius) {
		this.x = x;
		this.y = y;
		this.radius = radius;
	}
	
	public void zeichne() {
		System.out.println("Kreis mit Bezugspunkt: (" + x + "," + y
				+ "), Radius: " + radius);
	}

	public double getFlaeche() {
		return radius * radius * 3.14159;
	}
}