package Kap_3_Buch_Uebung;



public class U_24_Ringpuffer {
	
	private int[] array;
	private int idx;
	private boolean voll;
	
	
	public U_24_Ringpuffer (int length) {
		array = new int[length];
		idx = 0;
		voll = false;
	}
	

	public void schreibe(int v) {
		if (idx >= array.length)
			return;

		array[idx] = v;
		if (idx == array.length - 1)
			voll = true;
		idx = (idx + 1) % array.length;
	}
		
	
	public void lies() {
		if (!voll) {
			for (int i = 0; i < idx; i++)
				System.out.print(array[i] + " ");
		} else {
			for (int i = 0; i < array.length; i++)
				System.out.print(array[i] + " ");
		}
		
		System.out.println();
	}
}
