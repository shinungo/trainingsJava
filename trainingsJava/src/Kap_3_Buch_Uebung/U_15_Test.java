package Kap_3_Buch_Uebung;

public class U_15_Test {

	public static void main(String[] args) {
		U_15_Artikel artikel1 = new U_15_Artikel(4711, 1.99);
		U_15_Artikel artikel2 = new U_15_Artikel(5000, 10.5);

		U_15_Auftrag auftrag1 = new U_15_Auftrag(artikel1, 100);
		U_15_Auftrag auftrag2 = new U_15_Auftrag(artikel2, 50);

		double gesamtwert = U_15_Auftrag.getGesamtwert(auftrag1, auftrag2);
		System.out.println(gesamtwert);
	}
}
