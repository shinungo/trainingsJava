package Kap_3_Buch_Uebung;

public class U_16_Rechtek extends U_16_Figur {
	private int x, y, breite, hoehe;

	public U_16_Rechtek(int x, int y, int breite, int hoehe) {
		this.x = x; 
		this.y = y; 
		this.breite = breite; 
		this.hoehe = hoehe; 
	}
	
	public void zeichne() {
		System.out.println("Rechteck mit Bezugspunkt: (" + x + "," + y
				+ "), Breite: " + breite + ", Hoehe: " + hoehe);
	}

	
	
	@Override
	public double getFlaeche() {
			return breite * hoehe;
	}

}
