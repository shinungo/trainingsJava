package Kap_3_Buch_Uebung;

public interface U_19_Angestellter extends U_19_Person {

	double getGehalt();
	void setGehalt(double gehalt);
}
