package Kap_3_Buch_Uebung;

public class U_14_Enum {
	
	public enum Wochentag {
		MO(false), DI(false), MI(false), 
		DO(false), FR(false), SA(true), 
		SO(true);

		private boolean wochenende;

		private Wochentag(boolean w) {
			wochenende = w;
		}

		public boolean wochenende() {
			return wochenende;
		}
	}
}