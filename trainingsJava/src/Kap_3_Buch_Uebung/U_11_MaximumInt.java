package Kap_3_Buch_Uebung;

public class U_11_MaximumInt {
	
	public static void main(String[] args) {
		System.out.println(max(1, 100));
		System.out.println(max(5, -1, 4, 8, 7));
	}

	public static int max(int firstValue, int... remainingValues) {
		int max = firstValue;
		for (int i : remainingValues) {
			if (i > max)
				max = i;
		}
		
		return max;
	}

}
