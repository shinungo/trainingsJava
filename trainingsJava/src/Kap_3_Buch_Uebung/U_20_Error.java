package Kap_3_Buch_Uebung;

public class U_20_Error {
	
//	Warum terminiert der Aufruf von main in der unten aufgef�hrten Klasse B mit
//	einer NullPointerException

	
//	public class A {
//		public A() {
//			m();
//		}
//		public void m() {
//			System.out.println("m aus A");
//		}
//	}
//	
//	public class B extends A {
//		private A a;
//		
//		public B() {
//			a = new A();
//			m();   
//		}
//
//		
//		public void m() {
//			a.m();
//			System.out.println("m aus B");
//		}
//		public static void main(String[] args) {   //<<<<<<=== HIER WIRD ES SPANNEND!!
//			new B();
//		}
//	}

//	Antwort:
//
//		Die Anweisung 
//			new B(); 
//		f�hrt zun�chst zum Aufruf des Konstruktors von A.
//
//		Wegen dynamischer Bindung wird nicht die Methode m aus A, sondern die aus B aufgerufen.
//		Zu diesem Zeitpunkt ist aber die Variable a noch nicht initialisiert.
//		Also wird eine NullPointerException ausgel�st.
	
}
