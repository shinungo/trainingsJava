package Kap_3_Buch_Uebung;

public class U_12_Test {
		public static void main(String[] args) {
			U_12_ArrayIntegerList liste = new U_12_ArrayIntegerList(100);
			liste.insertLast(10);
			liste.insertLast(2);
			liste.insertLast(22);
			liste.insertLast(13);
			System.out.println(liste.getLength());
			System.out.println(liste.getFirst());

			liste.deleteFirst();

			System.out.println(liste.getLength());
			System.out.println(liste.getFirst());

			System.out.println(liste.search(22));
		}
	}