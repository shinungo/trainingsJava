package Kap_3_Buch_Uebung;

public class U_15_Artikel {
	
	private int id;
	private double preis;
	
	public U_15_Artikel(int id, double preis) {
		this.id = id; 
		this.preis = preis;
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}
}
