package Kap_3_Buch_Uebung;

import Kap_3_Buch_Uebung.U_14_Enum.Wochentag;

public class U_14_EnumTest {
	
		public static void main(String[] args) {
			for (Wochentag tag : Wochentag.values()) {
				System.out.print(tag.toString());
				if (tag.wochenende())
					System.out.println(" ist Wochenende");
				else
					System.out.println();
			}
		}
	}
