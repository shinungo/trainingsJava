package Kap_3_Buch_Uebung;

public class U_04_Sparbuch implements U_10_Anzeigbar {
	
	private int kontonummer; 
	private double kapital; 
	private double zinssatz; 
	
	public U_04_Sparbuch() {
	}
	
	public U_04_Sparbuch(int kontonummer) {
		this.kontonummer = kontonummer;
	}

	public U_04_Sparbuch(int kontonummer, double kapital, double zinssatz) {
		this.kontonummer = kontonummer;
		this.kapital = kapital; 
		this.zinssatz = zinssatz;	
	}
	
	
	
	public U_04_Sparbuch(U_04_Sparbuch sparbuch) {
		kontonummer = sparbuch.kontonummer;
		kapital = sparbuch.kapital;
		zinssatz = sparbuch.zinssatz;
	}
	

	
	
	public void zahleEin(int betrag) {
		this.kapital += betrag;
	}

	public void hebeAb(int betrag) {
		this.kapital -= betrag;
	}
	
	public void setzZinssatz(double zinssatz) {
		this.zinssatz = zinssatz;
	}
	
	

	public double getErtrag(int laufzeit) {
		
	
		for (int i = 0; i < laufzeit; i++) {
			kapital = kapital * (100 + zinssatz) / 100;
		}
		return kapital;
	}
	
	
	public void verzinse() {
		kapital = kapital * (100 + zinssatz) / 100;
	}
	
	
	
	public int getKontonummer() {
		return kontonummer;
		
	} 
	
	public double getKapital( ) {
		return kapital;
	}

	public double getZinssatz() {
		return zinssatz;
	}
	
	
	public void makeInfo() {
	
		System.out.println("Kaptial von Konto Nr." + getKontonummer() + " zu beginn:" + getKapital());
		
		hebeAb(100);
		System.out.println("auszahlung von 100: Saldo: " + getKapital() );
		
		zahleEin(999);
		System.out.println("Einzahlung von 999: Saldo: " + getKapital() );
		
		setzZinssatz(10.);
		getErtrag(5);
		System.out.println("Kaptial nach 5 Jahren und " +  getZinssatz() +"% Zins: " + getKapital() );
		
		verzinse();
		System.out.println("Kaptial nach 1 Jahren und " +  getZinssatz() +"% Zins: " + getKapital() );
	}
	
	public static void main (String[] args) {
		U_04_Sparbuch sparbuch = new U_04_Sparbuch(1234, 101, 0);
		sparbuch.makeInfo();
	}
	


	@Override
	public void zeige() {
		// TODO Auto-generated method stub
		System.out.println("Sparbuch");
		System.out.println("Kontonummer: " + kontonummer);
		System.out.println("Kapital: " + kapital);
		System.out.println("Zinssatz: " + zinssatz);
	}
}