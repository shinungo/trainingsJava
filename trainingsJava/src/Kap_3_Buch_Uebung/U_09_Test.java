package Kap_3_Buch_Uebung;

public class U_09_Test {


	public static void main (String[] args) {

		U_09_Azubi einAzubi = new U_09_Azubi("Schmitz", "Hugo", 1500.);
		U_09_Angestellter einAngestellter = new U_09_Angestellter("Schulze", "Erwin", 6000.);

		einAzubi.zeigeDaten();
		einAngestellter.zeigeDaten();

		einAzubi.setPruefungen(5);
		einAzubi.addZulage(100);
		einAngestellter.befoerdere();
		einAngestellter.erhoeheGehalt(500.);
		einAngestellter.befoerdere();
		einAngestellter.erhoeheGehalt(600.);
		einAngestellter.addZulage(200);

		einAzubi.zeigeDaten();
		einAngestellter.zeigeDaten();
	}		


}
