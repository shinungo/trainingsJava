package Kap_3_Buch_Uebung;

public class U_16_Test {
	
	public static void main (String[] args) {
			U_16_Figur[] figuren = new U_16_Figur[4];  
			figuren[0] = new U_16_Kreis(0, 0, 5);
			figuren[1] = new U_16_Rechtek(0,0,100,50); 
			figuren[2] = new U_16_Kreis(2,1, 10); 
			figuren[3] = new U_16_Rechtek(5,1,5,10);

				for (U_16_Figur f: figuren) {
					f.zeichne();
					System.out.println("Fl�che = " + f.getFlaeche());
					System.out.println();
				} 
	}
}
