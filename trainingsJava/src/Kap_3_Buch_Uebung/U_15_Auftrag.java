package Kap_3_Buch_Uebung;

public class U_15_Auftrag {
	
	private U_15_Artikel artikel; 
	private int menge; 
	
	
	public U_15_Auftrag(U_15_Artikel a, int m) {
		this.artikel = a;
		this.menge = m; 
	}

	public U_15_Artikel getArtikel() {
		return artikel;
	}

	public void setArtikel(U_15_Artikel artikel) {
		this.artikel = artikel;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

	public double getAuftragswert() {
		return menge * artikel.getPreis();
	}

	public static double getGesamtwert(U_15_Auftrag... auftraege) {
		double gesamt = 0;
		
		for (U_15_Auftrag a : auftraege) {
			gesamt += a.getAuftragswert();
		}
		
		return gesamt;
	}
}
