package Kap_3_Buch_Uebung;

public class U_21_Verzinsung2 {
	
	public static void main(String[] args) {
		System.out.println(zinsen(1000, 0.1, 5));  // �bergibt der Methode Zinsen Kaptial, Zins und Jahre. 
	}
	
	public static double zinsen(double kapital, double zinssatz, int jahre) {
		if (jahre == 0) {
			System.out.println("Schlaufe um Zins pro Jahr zu rechnen");
			return kapital;
		}
		System.out.println("Schluss: Kaptial zzgl. Zins");
	
		return zinsen(kapital, zinssatz, jahre - 1) * (1 + zinssatz);
	}
	
	
}
