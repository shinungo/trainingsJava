package Kap_3_Buch_Uebung;

public class U_22_Stack {
	private Node top; 

	
	public void push(int x) {
		Node p = new Node(); 
		p.data = x; 
		p.next = top; 
		top = p; 
		
	}
	
	
	
	public int pop() {
		if (top == null)
			return -1;
		
		Node p = top;
		top = top.next; 
		return p.data;
	}

	private class Node {
		private int data;	
		private Node next; }



}
