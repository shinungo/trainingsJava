package Kap_3_Buch_Uebung;

import java.util.ArrayList;
import java.util.List;

public class U_12_ArrayIntegerList implements U_12_IntegerList{
	private int[] array;
	private int length;
	
	
	public U_12_ArrayIntegerList(int max) {
		array = new int[max];
	}
	
	public int getLength() {
		return length;
	}

	public void insertLast(int value) {
		if (value < 0)
			return;
		
		array[length] = value;
		length++;
	}

	public int getFirst() {
		if (length > 0)
			return array[0];
		else
			return -1;
	}

	public void deleteFirst() {
		for (int i = 0; i < length; i++) {
			array[i] = array[i + 1];
		}
		
		length--;
	}

	public boolean search(int value) {
		for (int i = 0; i < length; i++) {
			if (array[i] == value)
				return true;
		}
		
		return false;
	}
}
