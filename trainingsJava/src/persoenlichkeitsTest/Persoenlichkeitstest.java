package persoenlichkeitsTest;
import java.util.Scanner;

public class Persoenlichkeitstest {
		public static String begruessung = "Bitte Schätzen Sie sich ein, auf einer Skala von \n1 - 7 (überhaupt nicht - voll und ganz) \n";
		public static String loesung = "";
		public static int i;
		public static int eingabe;	
		
		public static double exotro;
		public static double vertra;
		public static double gewissenh; 
		public static double emoStabi; 
		public static double offenh; 
		
		public static String one = "Wie extrovertiert oder begeisterungsfähig sind Sie?";
		public static String two = "Wie kritisch oder selbstüchtig sind Sie?";	
		public static String three = "Wie zuverlässig und diszipliniet sind Sie?";
		public static String four = "Wie ängstlich oder leicht aus der Fassung zu bringen sind Sie?";
		public static String five = "Wie offen für Neue Erfahrungen bzw. wie Vielschichtig sind Sie?";
		public static String six = "Wie zürickhaltend oder sill sind Sie?";
		public static String seven = "Wie verständnissvoll oder warmherzig sind Sie?";
		public static String eight = "Wie unorganisiert oder achtlos sind Sie?";
		public static String nine = "Wie gelassen oder emogional stabil sind Sie?";
		public static String ten = "Wie konventionell oder unkreative  sind Sie?";
	    	
	    
		public static void main (String[] args) {
			System.out.println(begruessung );
			fragegenerator();
		}
		
		public static void fragegenerator() {
			Scanner scanner = new Scanner(System.in);
			for ( i = 0; i < 10; i++) { 
				 	if (i == 0) {System.out.println(one);eingabe = scanner.nextInt();auswertung();}  
					if (i == 1) {System.out.println(two);eingabe = scanner.nextInt();auswertung();}
					if (i == 2) {System.out.println(three);eingabe = scanner.nextInt();auswertung();}
					if (i == 3) {System.out.println(four);eingabe = scanner.nextInt();auswertung();}
					if (i == 4) {System.out.println(five);eingabe = scanner.nextInt();auswertung();}
					if (i == 5) {System.out.println(six);eingabe = scanner.nextInt();auswertung();}
					if (i == 6) {System.out.println(seven);eingabe = scanner.nextInt();auswertung();}
					if (i == 7) {System.out.println(eight);eingabe = scanner.nextInt();auswertung();}
					if (i == 8) {System.out.println(nine);eingabe = scanner.nextInt();auswertung();}
					if (i == 9) {System.out.println(ten);eingabe = scanner.nextInt();auswertung();}
				}
			scanner.close();
			results();
		}

			public static void auswertung() {
				replace();
				calculate();
				}
				
			
			public static void replace() {
			if (i == 1 || i == 3 || i == 5 || i == 7 || i == 9) {
				switch(eingabe) {
				case 1: eingabe = 7;break;
				case 2: eingabe = 6;break;
				case 3: eingabe = 5;break;
				case 4: eingabe = 4;break;
				case 5: eingabe = 3;break;
				case 6: eingabe = 2;break;
				case 7: eingabe = 1;break;
				}
						}
	//		System.out.println("eingabe nach behandlung " + eingabe);
				}
			
			public static void calculate() {
			if (i == 0 || i == 5) {exotro = exotro + eingabe;}
			if (i == 1 || i == 6) {vertra = vertra + eingabe;}		
			if (i == 2 || i == 7) {gewissenh = gewissenh + eingabe;}
			if (i == 3 || i == 8) {emoStabi = emoStabi + eingabe;}
			if (i == 4 || i == 9) {offenh = offenh + eingabe;}
				}
			
			public static void results() {
				System.out.println("Durchschnittswert           Ich    Ø M     Ø F");
				System.out.println("Extrovertiertheit         : " + (exotro/2)+"    3.8     4.1");
				System.out.println("Verträglichkeit           : " + (vertra/2)+"    4.7     5.1");
				System.out.println("Gewissenhaftigkeit        : " + (gewissenh/2)+"    4.8     5.0");
				System.out.println("Emmotioal Stabilität      : " + (emoStabi/2)+"    4.7     4.4");
				System.out.println("Offenheit für Erfahrungen : " + (offenh/2)+"    5.4     5.5");
			}
			

			
			/*
			 * Ersetzten sie bei den Fragen 2,4,6,8 und 10 die Zahlen Ihrer Einschätzung:
			 * Und zwar die 1 mit einer 7 
			 * die 2 mit einer 6, 
			 * die 3 mit einer 5, 
			 * die 4 bleibt 4, 
			 * die 5 mit einer 3, 
			 * die 6 mit einer 2, 
			 * die 7 mit einer 1,
			 * 
			 * addieren Sie nach dieser Korrekutr die Werte der folgenen Antwortpaare und teilen Sie durch 2: 
			 * 
			 * 1 + 6 = ____ / 2 = Exotrovertiertheit
			 * 2 + 7 = ____ / 2 = Verträglichkeit
			 * 3 + 8 = ____ / 2 = Gewissenhaftigkeit
			 * 4 + 9 = ____ / 2 = Emmotionale Stabilität
			 * 5 + 10 = ____ / 2 = Offenheit für Erfahrungen 
			 * 
			 * Die Position in den fünf Persönlichkeitsvereichen können Sie mit 
			 * den Untentstehenden Durchschnittswerten vergelichen, 
			 * oder mit jenen anderen Personen, die den Test machen
			 * 
			 *  Durchschnittswert 			Männer	Frauen Ich 
			 *  Exotrovertiertheit			3.8		4.1
			 *  Verträglichkeit				4.7		5.1
			 *  Gewissenhaftigkeit			4.8		5.0
			 *  Emmotionale Stabilität		4.7		4.4
			 *  Offenheit für Erfahrungen	5.4		5.5
			 */
		
		public static void persoenlichkeitsprofil() {
			
			/*
			 * SIEHE FOTO
			 * Selbstwahrnehmung
			 * Spontan
			 * Gefühlsbetont
			 * innovativ
			 * geniesserisch
			 * strategisch
			 * entschlussfreudig
			 * risikofreudig
			 * antriebsstarkt
			 * sozial
			 * optimistisch
			 * ruhig
			 * stresstolerant
			 * selbstsicher
			 */
		}
	}