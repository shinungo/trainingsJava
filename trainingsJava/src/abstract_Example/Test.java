package abstract_Example;



/**
 * Die abstrakte Klasse Zeit implementiert die Methode getSekunden 
 * und bedient sich dabei der abstrakten Methode getMinuten. 
 * Die von der abstrakten Klasse Zeit abgeleiteten Klassen Tage und 
 * StundenMinuten überschreiben jeweils die abstrakte Methode getMinuten 
 * durch eine konkrete Implementierung. Beim Aufruf der Methode getSekunden 
 * in der Klasse Test wird nun die zum Typ des Objekts passende 
 * Implementierung von getMinuten ausgeführt.
 */





public class Test {
public static void main(String[] args) {
	
	Zeit z = new Tage(3); 
	System.out.println(z.getSekunden());
	
	z = new StundenMinuten(8, 30);
	
	System.out.println(z.getSekunden()); }


/**
 * Die Variable z im Beispiel zeigt im Verlauf des Tests auf 
 * Objekte verschiedener Klassen. Zunächst wird die 
 * getSekunden-Methode für ein Tage-Objekt aufgerufen, dann 
 * für ein StundenMinuten-Objekt. Dasselbe Programmkonstrukt 
 * ist also für Objekte mehrerer Typen einsetzbar.
 */

}