package instanzklasse;

public class Test {


	public static void main(String[] args) {

		Konto k = new Konto(4711, 1000.);
		k.zahleEin(500.);
		k.zahleAus(700.);

		Konto.Transaktion t = k.getLast();
		System.out.println(t.toString()); 

	}
}