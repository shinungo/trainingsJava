package getIntFromAList;

import java.util.ArrayList;
import java.util.List;

public class GetStuffFromList {
	
	private List<Integer> liste = new ArrayList<Integer>();
	private int aa = 11; 
	private int bb = 21; 
	private int cc = 23; 
	private int dd = 44; 
	private int ee = 15; 
	private int ff = 86; 
	private int gg = 77; 
	private int hh = 57; 
	private int ii = 58; 


	public GetStuffFromList() {
		liste.add(aa);
		liste.add(bb);
		liste.add(cc);
		liste.add(dd);
		liste.add(ee);
		liste.add(ff);
		liste.add(gg);
		liste.add(hh);
		liste.add(ii);
	}
	
	/*
	 * Hier hole ich mit dem Index der Liste jedes Element. 
	 * Wenn das Element TRUE (geteilt-durch-ohne-Rest) ergibt, 
	 * dann printe es.  
	 */
	
	public void showContent() {
		for (int i = 0; i < liste.size();i++){
			if (liste.get(i) % 2 == 0) {
				System.out.println(" Gefilterte, ungerade Zahl " +  liste.get(i));
			}
		}
	}

	public static void main (String[] Args) {
		GetStuffFromList get = new GetStuffFromList();
		get.showContent();
	}
}
