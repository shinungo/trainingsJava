package interfaceLifeCycle;

public interface Lifecycle { 
		
	void start();
	void stop();
    
	// init und Destroy sind neu
	
	
	default void init() {
      }
    
	
	default void destroy() {
		} 
	
}