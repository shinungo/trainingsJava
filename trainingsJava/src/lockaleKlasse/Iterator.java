package lockaleKlasse;

public interface Iterator { 
	boolean hasNext();
	Object next();
}
