package fibunacci;

import java.text.NumberFormat;
import java.util.Scanner;

public class Fibunacci {
	
private static double last, before,  i = 0;
private static String print;
private static int counter = 0;
private static double beginn = 1;
static Scanner sc = new Scanner(System.in); 

// Was it me?

	public static void main(String[] args) {
		System.out.println("Gib eine Zahl zur Beschränkung der Fibunacci Loops ein! ");
		i = sc.nextDouble(); 
		makeALoop();
		System.out.println("... es wurde "+i+" mal gerechnet.");
		}
	
	public static void makeALoop() {
		while (counter < i) {
			System.out.println("Durchgang Nr." + counter + " Zahl " + print); 
			
			NumberFormat formatter = NumberFormat.getInstance();
			formatter.setMaximumFractionDigits(2);
			
			before = last;			
			last = last + beginn;
			print = formatter.format(last);
			beginn = last - beginn; 	
			counter++;	
		}; 
	}
}