package innereKlasse;

public class Test {

	public static void main(String[] args) {
		Account account = new Account(4711);
		Account.Permissions perm = account.getPermissions(); 
		perm.canRead = true;

		System.out.println(perm.canRead); 
		System.out.println(perm.canWrite); 
		System.out.println(perm.canDelete);
	} 
}