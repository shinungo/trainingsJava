package innereKlasse;


public class Account { 

	private int userId; 
	private Permissions perm;
	public Account(int userId) { 
		this.userId = userId;
		perm = new Permissions(); 
	}

	public int getUserId() {
		return userId;
	}

	public static class Permissions { 
		public boolean canRead;
		public boolean canWrite; 
		public boolean canDelete;
	}

	public Permissions getPermissions() {
		return perm;
	}
}

