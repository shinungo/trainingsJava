package Kap_4_Buch_Uebung;

public class U_3_Test {

	public static void main(String[] args) {
		try {
			U_3_Monat m1 = new U_3_Monat(2);
			System.out.println(m1.getMonatsname());
			U_3_Monat m2 = new U_3_Monat(13);
			System.out.println(m2.getMonatsname());
		} catch (U_3_MonatAusnahme e) {
			System.out.println(e.getMessage());
		}
	}
}
