package Kap_4_Buch_Uebung;

public class U_3_Monat  {

	private int monat;
	
	
	public U_3_Monat(int monat) throws U_3_MonatAusnahme { 
		if (monat <1 || monat > 12) {
			throw new U_3_MonatAusnahme(monat);
		}
		this.monat = monat;
	}
	
	public String getMonatsname() {
		switch (monat) {
		case 1:
			return "Januar";
		case 2:
			return "Februar";
		case 3:
			return "M�rz";
		case 4:
			return "April";
		case 5:
			return "Mai";
		case 6:
			return "Juni";
		case 7:
			return "Juli";
		case 8:
			return "August";
		case 9:
			return "September";
		case 10:
			return "Oktober";
		case 11:
			return "November";
		case 12:
			return "Dezember";
		default:
			return null;
		}
	}
	
}
