package Kap_4_Buch_Uebung;

public class  U_3_MonatAusnahme extends Exception {
	
	
	public U_3_MonatAusnahme() {
	}

	public U_3_MonatAusnahme(int monat) {
		super(monat + " ist kein Monat");
	}
}
