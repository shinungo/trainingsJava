package Kap_2_Buch;

public class U_17_Reihen_Spalten {
	
	
/*
 * 	Schreiben Sie ein Programm, das eine Tabelle mit dem kleinen Einmaleins 
 * (also 1 * 1 bis 10 * 10) angeordnet in zehn Zeilen mit je zehn Spalten ausgibt.
 */
	
	public static void main(String[] args) {

		
		int anzahl = 11;
		int k = 10; 

		Hauptschleife : for (int i = 1; i < anzahl; i++) {
	
			for (int j = 1; j< anzahl; j++) {
				System.out.print(j + "*" + i + "  ");


				if ( j ==  k) {
					System.out.println();
					continue Hauptschleife;
				}
			}
		}
	}

}