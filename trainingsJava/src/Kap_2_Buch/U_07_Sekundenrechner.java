package Kap_2_Buch;

public class U_07_Sekundenrechner {

	public static void main (String[] args) {
		
		int tage = 31;
		int stunden = 24;
		int minuten = 60;
		int sekunden = 60; 
		
		int sekundenzahl = sekunden * minuten * stunden * tage;
		
		System.out.println("der Januar hat " + sekundenzahl + " Sekunden");
		
		
		// MUSTERLöSUNG: 
		
		long sek;

		sek = 31 * 24 * 60 * 60;
		System.out.println("Der Monat Januar hat " + sek + " Sekunden.");
		
		
		
		
	}
}
