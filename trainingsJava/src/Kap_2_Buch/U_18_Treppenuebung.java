package Kap_2_Buch;

public class U_18_Treppenuebung {

	public static void main(String[] args) {
	
		int breite = 3; 
		int hoehe = 10;


		Schleife : for (int i = 0; i < hoehe; i++ ) {
			for (int j = 0;j < breite ;j++) {
				System.out.print("---");

				if (j == i)  {
					System.out.println();
					breite++;
					continue Schleife;
				}
			}

		}
	}
}

// LOESUNGSVORSCHLAG: 

//	public static void main(String[] args) {
//		int h = 10;
//		int b = 3;
//
//		for (int i = 1; i <= h; i++) {
//			for (int j = 1; j <= h - i; j++) {
//				for (int k = 1; k <= b; k++) {
//					System.out.print(" ");
//				}
//			}
//			
//			for (int j = 1; j <= i; j++) {
//				for (int k = 1; k <= b; k++) {
//					System.out.print("-");
//				}
//			}
//			
//			System.out.println();
//		}
//	}
//}