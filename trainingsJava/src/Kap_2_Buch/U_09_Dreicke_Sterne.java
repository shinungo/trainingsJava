package Kap_2_Buch;

public class U_09_Dreicke_Sterne {

	//	Schreiben Sie ein Programm, das eine von Ihnen vorgegebene Anzahl 
	//	von Sternen (*) in Form eines Dreiecks auf dem Bildschirm ausgibt.

	public static void main(String[] args) {

		int anzahl = 7; 

		Hauptschleife : for (int i = 0; i < anzahl; i++) {
			for (int j = 0; j< anzahl; j++) {
				System.out.print("*" + " ");


			
				if ( j ==  i) {
					System.out.println();
					continue Hauptschleife;
				}
			}
		}
	}

}


// MUSTERLöSUNG: 

//public class Sterne {
//	public static void main(String[] args) {
//		int zeile, spalte;
//		
//		for (zeile = 1; zeile <= 10; zeile++) {
//			for (spalte = 1; spalte <= zeile; spalte++) {
//				System.out.print('*');
//			}
//			
//			System.out.println();
//		}
//	}
//}
