package Kap_2_Buch;

public class U_10_Sparguthaben {

	
//	Schreiben Sie ein Programm, das ermittelt, wie hoch ein Sparguthaben von 5.000 
//	Geldeinheiten bei 7,5 % Verzinsung nach Ablauf eines Jahres ist.


	public static void main(String[] args) {
		
		double guthaben = 5000;
		double zins = 7.5;
	
		// MEINE VERSION: 
		//	guthaben = guthaben + (guthaben / 100 * zins);
		
		// Lösgungsvorschalgt: 
		
		guthaben += guthaben / 100 * zins;
		
		
		System.out.println("Guthaben bei "+ zins + "% Zins nach einem Jahr " + guthaben);
		
	}

}
