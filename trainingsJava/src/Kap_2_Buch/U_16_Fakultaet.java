package Kap_2_Buch;


public class U_16_Fakultaet {

	/*
	 * Schreiben Sie ein Programm, das zu einer 
	 * Zahl n <= 20 die Fakultät n! ermittelt. 
	 * Es gilt bekanntlich:
	 *n! = 1 * 2 * ... * (n-1) * n sowie 0! = 1
	 */

	public static void main (String[] args) {
		double n = 21;   // int bis 12 ist ok
		double ergebnis = 1; 

		Haupt : for (int j = 0; j < n;  ) 
		{
			j++;
			ergebnis = ergebnis * j;
			continue Haupt; 
		}
		System.out.println(ergebnis);
		
		
		//MUSTER Lösung: 
		
		int nn = 21; // Überlauf, wenn n > 20

		long fak = 1;
		for (int i = 1; i <= nn; i++) {
			fak *= i;
		}

		System.out.println(fak);
		
	}
}