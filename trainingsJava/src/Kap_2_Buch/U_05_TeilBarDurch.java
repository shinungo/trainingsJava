package Kap_2_Buch;

// Zu vorgegebenen Zahlen x und y, soll festgestellt werden, ob x durch y teilbar ist. 


public class U_05_TeilBarDurch {
	
	public static void main (String[] args) {
		int x = 123456789;
		int y = 3; 
		int resultat = (x % y);
		
		if (resultat == 0) {
			System.out.println("Das Resultat ist " + resultat + " und somit teilbar"); 
		} else {
			System.out.println("Das Resultat ist " + resultat + " und nicht teilbar");	
		}
	
	// MUSTERLöSUNG: 
	
		int x1 = 123456789;
		int y2 = 3;

		String ergebnis = (x1 % y2 == 0) ? "teilbar" : "nicht teilbar";

		System.out.println(x1 + " ist durch " + y2 + " " + ergebnis + ".");
	
	}
	
}
