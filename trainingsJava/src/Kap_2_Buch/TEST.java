package Kap_2_Buch;

public class TEST {

		 /*
		  * Berechnung des ggT zweier Zahlen 
		  * nach dem Euklidischen Algorithmus
		  */
		 private static int ggt(int zahl1, int zahl2) {
		   while (zahl2 != 0) {
		     if (zahl1 > zahl2) {
		       zahl1 = zahl1 - zahl2;
		     } else {
		       zahl2 = zahl2 - zahl1;
		     }
		   }
		   return zahl1;
		 }
		 
		 /*
		  * Hauptprogramm:
		  */
		 public static void main(String[] args) {
		   /*
		    * Kommandozeilenargumente einlesen
		    * Aufruf: "ggt <zahl1> <zahl2>"
		    */
		   int ersteZahl = Integer.parseInt(args[0]);
		   int zweiteZahl = Integer.parseInt(args[1]);
		 
		   // berechne ggT mit der Funktion "ggt()"
		   int ergebnis = ggt(ersteZahl, zweiteZahl);
		 
		   // Ausgabe des Ergebnisses:
		   System.out.println("Der ggT von "  + ersteZahl + 
		       " und " + zweiteZahl + " ist: " + ergebnis);
		 }
		}