package Kap_2_Buch;


//Programm zeigt, dass beim Rechnen mit double-Werten Genauigkeit verloren gehen kann. 
//Das Programm soll die folgende Aufgabe lösen:
//Sie haben einen Euro und sehen ein Regal mit Bonbons, 
// die 10 Cent, 20 Cent, 30 Cent usw. bis hinauf zu einem Euro kosten. 
//Sie kaufen von jeder Sorte ein Bonbon, beginnend mit dem Bonbon für 10 Cent, 
// bis Ihr Restgeld für ein weiteres Bonbon nicht mehr ausreicht. 
// Wie viele Bonbons kaufen Sie und welchen Geldbetrag erhalten Sie zurück?


public class Theorie_Bonbons1 {
	public static void main(String[] args) {
		double budget = 1; int anzahl = 0;
		for (double preis = 0.1; budget >= preis; preis += 0.1) { budget -= preis;
		anzahl++; }
		System.out.println(anzahl + " Bonbons gekauft.");
		System.out.println("Restgeld: " + budget); }
}
