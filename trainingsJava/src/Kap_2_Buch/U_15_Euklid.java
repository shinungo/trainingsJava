package Kap_2_Buch;

public class U_15_Euklid {

	public static void main (String[] args) {
		int p = 4444;
		int q = 448;

		// HIER WERDEN DIE ZAHLEN IN DIE GRÖSSEN -REIHENFOLGE GEBRACHT 
		
		if (p < q) {
			int h = p;   	// h = 10
			p = q;			// p = 40
			q = h;			// q = 10
		}
		
		

		int r;
		while (q != 0) {
			r = p % q;     	// r = modulo VON P/Q - Es bleibt der nicht teilbare Rest der Division. 
			System.out.println(r);
			p = q;
			q = r;
		}

		System.out.println(p);
	}
}