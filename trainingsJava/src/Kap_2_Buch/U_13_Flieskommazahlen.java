package Kap_2_Buch;

//
//Zwei Fließkommazahlen sollen verglichen werden. Ist der Absolutbetrag 
//der Differenz dieser beiden Zahlen kleiner als ein vorgegebener Wert 
//z, soll 0 ausgegeben werden, 

//sonst -1 bzw. 1, 
//je nachdem x kleiner als y oder größer als y ist.

public class U_13_Flieskommazahlen {
	public static void main (String[] args) {

		float x = 9; 
		float y = 51; 

		float differenz = x -y;  
		float absolut = (Math.abs(differenz)); 

		float vorgabeZ = 1;
		System.out.println(absolut);


		if (absolut < vorgabeZ) {
			System.out.println("0"); 
		} else if (x > y) {
			
			System.out.println("1 - x ist Grösser");		
		} else {
			
			System.out.println("-1 - y ist Grösser");	
		}
	
	
	// MUSTER Lösung. 
	double xx = 1.23;
	double yy = 1.234;
	double z = 0.01;
	
	double abs = (xx - yy < 0) ? yy - xx : xx - yy;
	
	if (abs < z)
		System.out.println(0);
	else if (xx < yy)
		System.out.println(-1);
	else
		System.out.println(1);
}
	
}