package Kap_2_Buch;

public class U_04_FlaschenInKarton {

	public static void main (String [] args) {
		
		// TEST 23.3.2020
		
		int flasche = 123;
		int anzahlKarton = 0;
	
		
		for (int karton = 10; flasche >= karton; ) 
			{
				flasche -= karton; 
				anzahlKarton++;
					}
		
		System.out.println(anzahlKarton + " Kartons mit Flasche");
		System.out.println("Restliche Flaschen: " + flasche);	
		
		// MUSTERLöSUNG: 
		
		int x = 123;
		int n = 10;
		
		System.out.println("Anzahl Kartons: " + x / n);
		System.out.println("Rest: " + x % n);
		
		
	}
}