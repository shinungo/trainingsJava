package Kap_2_Buch;

public class U_14_BitOperatoren {

	/*
	 * 
	 * Schreiben Sie ein Programm, das eine ganze Zahl 
	 * vom Typ int in Binär- darstellung (32 Bit) ausgibt. 
	 * Benutzen Sie hierzu die Bitoperatoren & und <<.
	 * 
	 * Tipp: 
	 * Das Bit mit der Nummer i (Nummerierung 
	 * beginnt bei 0) in der Binärdarstellung von zahl 
	 * hat den Wert 1 genau dann, wenn der Ausdruck 
	 * zahl & (1 << i) von 0 verschieden ist.
	 * 
	 * 
	 */
	
	public static void main(String[] args) {
		int zahl = 25235;

		for (int i = 31; i >= 0; --i) {
			if ((zahl & (1 << i)) != 0)
				System.out.print('1');
			else
				System.out.print('0');
			if (i % 8 == 0)
				System.out.print(' ');
		}

		System.out.println();
	}
}
