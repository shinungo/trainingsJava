package Kap_2_Buch;

public class U_21_Quersumme {

	public static void main(String[] args) {
		int x = 15;
		int quersumme = 0;
		
		while (x > 0) {
			quersumme = quersumme + x % 10;
			System.out.println(" x: " + x + " quersumme " + quersumme);
			x = x / 10;
		}
		
		System.out.println("Quersumme: " + quersumme);
	}
}