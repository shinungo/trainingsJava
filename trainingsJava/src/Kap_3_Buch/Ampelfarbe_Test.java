package Kap_3_Buch;

public class Ampelfarbe_Test {
	
	public static void info (Ampelfarbe farbe) {
		
		switch(farbe) {
		case ROT: System.out.println(farbe.ordinal() + " anhalten");
		break;
		
		case GELB: System.out.println(farbe.ordinal() + " warten ");
		break;
		
		case GRUEN: System.out.println(farbe.ordinal() + " fahren ");
		break;
		}
	}
	
	public static void main (String[] args) {
		
		info(Ampelfarbe.GELB);		
		info(Ampelfarbe.ROT);
		info(Ampelfarbe.GRUEN);
		
		for (Ampelfarbe farbe : Ampelfarbe.values()) {
			System.out.println(farbe.toString());
		}
	}
}