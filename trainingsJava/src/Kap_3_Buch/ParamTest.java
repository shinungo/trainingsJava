package Kap_3_Buch;

public class ParamTest {

	public void testMachWas(double betrag, Konto kto) {
	
		betrag += 100.;

		kto.zahleEin(betrag); }

	public static void main(String[] args) { 
		
		/*
		 * Bei Aufruf der Methode main wird ein neuer ParamTest erzeugt. 
		 * Ein ParamTest beihaltet eine Kontonummer und ein Wert.  
		 */
		
		ParamTest p = new ParamTest();

		double wert = 1000.;
		Konto konto = new Konto();

		System.out.println("Vorher: wert=" + wert + " saldo=" + konto.getSaldo());

		p.testMachWas(wert, konto);

		System.out.println("Nachher: wert=" + wert + " saldo=" + konto.getSaldo());
	} 
}