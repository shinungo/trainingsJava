package Kap_3_Buch;



/**
 * 
 * @author markusbolliger
 * 
 *  In Terminal zu starten mit: 
 *  java -cp bin Kommandozeile Dies ist ein "T e s t"
 * 
 *
 */

public class Kommandozeile {
	public static void main (String[] args) {
		for (int i = 0; i < args.length; i++) {
			System.out.println ((i + 1) + ". Parameter: " + args[i]);
		}

	}
}