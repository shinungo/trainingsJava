package interfaceBeispiel;

public class Test {
	
	
	/**
	 * Programm 3.11 zeigt die Mächtigkeit des Interface-Konzepts. 
	 * Die Klassen Rechteck und Kreis implementieren die im Interface 
	 * Geo vorgegebene Methode getFlaeche. Die Methode vergleiche der 
	 * Klasse GeoVergleich vergleicht zwei beliebige Objekte vom Typ Geo 
	 * anhand der Größe ihres Flächeninhalts miteinander. 
	 * Die Methode muss nicht "wissen", ob es sich um ein Rechteck oder 
	 * einen Kreis handelt. Sie ist also universell einsetzbar für alle Klassen, 
	 * die das Interface Geo implementieren.
	 * 
	 * 
	 */
	
	public static void main(String[] args) {
			Rechteck r = new Rechteck(10.5, 4.799); 
			Kreis k = new Kreis(4.0049);
				
			System.out.println(r.getFlaeche()); 
			System.out.println(k.getFlaeche()); 
			System.out.println(GeoVergleich.vergleiche(r, k));
			
} }