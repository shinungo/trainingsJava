package interfaceBeispiel;

public class Kreis implements Geo {

	private double radius;
	private static final double PI = 3.14159;
	public Kreis(double radius) {

		this.radius = radius;
	}

	@Override
	public double getFlaeche() {
		return PI * radius * radius; }	
}